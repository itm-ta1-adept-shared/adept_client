# coding: utf-8

"""
    ITM TA1 API

    This is the specification of the TA1 API for In The Moment (ITM) for the ADEPT server. This is created in support of the Dry Run Eval. It is used to characterize and determine alignment of a decision maker's choices.

    The version of the OpenAPI document: 3.0.1
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


from __future__ import annotations
import pprint
import re  # noqa: F401
import json

from pydantic import BaseModel, ConfigDict, Field
from typing import Any, ClassVar, Dict, List, Optional, Union
from typing_extensions import Annotated
from openapi_client.models.kdma_kdes import KDMAKdes
from openapi_client.models.kdma_name import KDMAName
from typing import Optional, Set
from typing_extensions import Self

class KDMA(BaseModel):
    """
    Single KDMA value with values between 0 and 1, or (not used) a kernel density estimate of the KDMA value.
    """ # noqa: E501
    kdma: KDMAName
    value: Optional[Union[Annotated[float, Field(le=1, strict=True, ge=0)], Annotated[int, Field(le=1, strict=True, ge=0)]]] = None
    kdes: Optional[KDMAKdes] = None
    __properties: ClassVar[List[str]] = ["kdma", "value", "kdes"]

    model_config = ConfigDict(
        populate_by_name=True,
        validate_assignment=True,
        protected_namespaces=(),
    )


    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.model_dump(by_alias=True))

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        # TODO: pydantic v2: use .model_dump_json(by_alias=True, exclude_unset=True) instead
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Optional[Self]:
        """Create an instance of KDMA from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self) -> Dict[str, Any]:
        """Return the dictionary representation of the model using alias.

        This has the following differences from calling pydantic's
        `self.model_dump(by_alias=True)`:

        * `None` is only added to the output dict for nullable fields that
          were set at model initialization. Other fields with value `None`
          are ignored.
        """
        excluded_fields: Set[str] = set([
        ])

        _dict = self.model_dump(
            by_alias=True,
            exclude=excluded_fields,
            exclude_none=True,
        )
        # override the default output from pydantic by calling `to_dict()` of kdes
        if self.kdes:
            _dict['kdes'] = self.kdes.to_dict()
        return _dict

    @classmethod
    def from_dict(cls, obj: Optional[Dict[str, Any]]) -> Optional[Self]:
        """Create an instance of KDMA from a dict"""
        if obj is None:
            return None

        if not isinstance(obj, dict):
            return cls.model_validate(obj)

        _obj = cls.model_validate({
            "kdma": obj.get("kdma"),
            "value": obj.get("value"),
            "kdes": KDMAKdes.from_dict(obj["kdes"]) if obj.get("kdes") is not None else None
        })
        return _obj


