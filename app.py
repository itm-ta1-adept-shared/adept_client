import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost:8080"
)



# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    target_id = 'ADEPT-DryRun-Ingroup Bias-0.0' # str | 

    try:
        # Get Alignment Target
        api_response = api_instance.get_alignment_target_api_v1_alignment_target_target_id_get(target_id)
        print("The response of DefaultApi->get_alignment_target_api_v1_alignment_target_target_id_get:\n")
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_alignment_target_api_v1_alignment_target_target_id_get: %s\n" % e)
