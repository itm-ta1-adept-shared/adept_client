# InjuryLocationEnum

the injury location on the character's body

## Enum

* `RIGHT_FOREARM` (value: `'right forearm'`)

* `LEFT_FOREARM` (value: `'left forearm'`)

* `RIGHT_HAND` (value: `'right hand'`)

* `LEFT_HAND` (value: `'left hand'`)

* `RIGHT_LEG` (value: `'right leg'`)

* `LEFT_LEG` (value: `'left leg'`)

* `RIGHT_CALF` (value: `'right calf'`)

* `LEFT_CALF` (value: `'left calf'`)

* `RIGHT_THIGH` (value: `'right thigh'`)

* `LEFT_THIGH` (value: `'left thigh'`)

* `RIGHT_STOMACH` (value: `'right stomach'`)

* `LEFT_STOMACH` (value: `'left stomach'`)

* `RIGHT_BICEP` (value: `'right bicep'`)

* `LEFT_BICEP` (value: `'left bicep'`)

* `RIGHT_SHOULDER` (value: `'right shoulder'`)

* `LEFT_SHOULDER` (value: `'left shoulder'`)

* `RIGHT_SIDE` (value: `'right side'`)

* `LEFT_SIDE` (value: `'left side'`)

* `RIGHT_CHEST` (value: `'right chest'`)

* `LEFT_CHEST` (value: `'left chest'`)

* `CENTER_CHEST` (value: `'center chest'`)

* `RIGHT_WRIST` (value: `'right wrist'`)

* `LEFT_WRIST` (value: `'left wrist'`)

* `LEFT_FACE` (value: `'left face'`)

* `RIGHT_FACE` (value: `'right face'`)

* `LEFT_NECK` (value: `'left neck'`)

* `RIGHT_NECK` (value: `'right neck'`)

* `INTERNAL` (value: `'internal'`)

* `HEAD` (value: `'head'`)

* `NECK` (value: `'neck'`)

* `STOMACH` (value: `'stomach'`)

* `UNSPECIFIED` (value: `'unspecified'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


