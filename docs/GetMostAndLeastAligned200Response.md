# GetMostAndLeastAligned200Response


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**most_aligned** | **str** |  | [optional] 
**least_aligned** | **str** |  | [optional] 

## Example

```python
from openapi_client.models.get_most_and_least_aligned200_response import GetMostAndLeastAligned200Response

# TODO update the JSON string below
json = "{}"
# create an instance of GetMostAndLeastAligned200Response from a JSON string
get_most_and_least_aligned200_response_instance = GetMostAndLeastAligned200Response.from_json(json)
# print the JSON string representation of the object
print(GetMostAndLeastAligned200Response.to_json())

# convert the object into a dict
get_most_and_least_aligned200_response_dict = get_most_and_least_aligned200_response_instance.to_dict()
# create an instance of GetMostAndLeastAligned200Response from a dict
get_most_and_least_aligned200_response_from_dict = GetMostAndLeastAligned200Response.from_dict(get_most_and_least_aligned200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


