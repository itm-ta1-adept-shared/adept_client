# InjurySeverityEnum

The severity of the injury; for revelant injuries, impacts blood pool sizes

## Enum

* `MINOR` (value: `'minor'`)

* `MODERATE` (value: `'moderate'`)

* `SUBSTANTIAL` (value: `'substantial'`)

* `MAJOR` (value: `'major'`)

* `EXTREME` (value: `'extreme'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


