# CommunicationCapabilityEnum

current availability of internal and external communication

## Enum

* `INTERNAL` (value: `'internal'`)

* `EXTERNAL` (value: `'external'`)

* `BOTH` (value: `'both'`)

* `NEITHER` (value: `'neither'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


