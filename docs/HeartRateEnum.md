# HeartRateEnum

Descriptive heart rate

## Enum

* `NONE` (value: `'NONE'`)

* `FAINT` (value: `'FAINT'`)

* `NORMAL` (value: `'NORMAL'`)

* `FAST` (value: `'FAST'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


