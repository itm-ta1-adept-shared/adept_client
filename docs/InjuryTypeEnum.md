# InjuryTypeEnum

A brief but descriptive label for the injury type

## Enum

* `EAR_BLEED` (value: `'Ear Bleed'`)

* `ASTHMATIC` (value: `'Asthmatic'`)

* `LACERATION` (value: `'Laceration'`)

* `PUNCTURE` (value: `'Puncture'`)

* `SHRAPNEL` (value: `'Shrapnel'`)

* `CHEST_COLLAPSE` (value: `'Chest Collapse'`)

* `AMPUTATION` (value: `'Amputation'`)

* `BURN` (value: `'Burn'`)

* `ABRASION` (value: `'Abrasion'`)

* `BROKEN_BONE` (value: `'Broken Bone'`)

* `INTERNAL` (value: `'Internal'`)

* `TRAUMATIC_BRAIN_INJURY` (value: `'Traumatic Brain Injury'`)

* `OPEN_ABDOMINAL_WOUND` (value: `'Open Abdominal Wound'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


