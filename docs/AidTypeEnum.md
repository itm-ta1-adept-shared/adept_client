# AidTypeEnum

Types of aid

## Enum

* `LOCAL_MILITARY` (value: `'local military'`)

* `LOCAL_NON_MINUS_MILITARY` (value: `'local non-military'`)

* `AIR_EVAC` (value: `'air evac'`)

* `GROUND_EVAC` (value: `'ground evac'`)

* `WATER_EVAC` (value: `'water evac'`)

* `UNKNOWN_EVAC` (value: `'unknown evac'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


