# openapi_client.DefaultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**compare_sessions**](DefaultApi.md#compare_sessions) | **GET** /api/v1/alignment/compare_sessions | Compare Sessions
[**computed_kdma_profile**](DefaultApi.md#computed_kdma_profile) | **GET** /api/v1/computed_kdma_profile | Compute KDMA Profile
[**get_alignment_target_api_v1_alignment_target_target_id_get**](DefaultApi.md#get_alignment_target_api_v1_alignment_target_target_id_get) | **GET** /api/v1/alignment_target/{target_id} | Get Alignment Target
[**get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get**](DefaultApi.md#get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get) | **GET** /api/v1/alignment_target_distribution/{target_id} | Get Alignment Target Distribution
[**get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get**](DefaultApi.md#get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get) | **GET** /api/v1/alignment_target_distribution_ids | Get alignment targets distribution IDs
[**get_alignment_target_ids_api_v1_alignment_target_ids_get**](DefaultApi.md#get_alignment_target_ids_api_v1_alignment_target_ids_get) | **GET** /api/v1/alignment_target_ids | Get alignment targets IDs
[**get_api_api_v1_get**](DefaultApi.md#get_api_api_v1_get) | **GET** /api/v1 | Get Api
[**get_most_and_least_aligned**](DefaultApi.md#get_most_and_least_aligned) | **GET** /api/v1/get_most_and_least_aligned | Get Most and Least Aligned Targets
[**get_probe_response_alignment_api_v1_alignment_probe_get**](DefaultApi.md#get_probe_response_alignment_api_v1_alignment_probe_get) | **GET** /api/v1/alignment/probe | Get Probe Response Alignment
[**get_scenario_api_v1_scenario_scenario_id_get**](DefaultApi.md#get_scenario_api_v1_scenario_scenario_id_get) | **GET** /api/v1/scenario/{scenario_id} | Get Scenario
[**get_session_alignment_api_v1_alignment_session_get**](DefaultApi.md#get_session_alignment_api_v1_alignment_session_get) | **GET** /api/v1/alignment/session | Get Session Alignment
[**post_new_session_id_api_v1_new_session_post**](DefaultApi.md#post_new_session_id_api_v1_new_session_post) | **POST** /api/v1/new_session | Post New Session Id
[**post_probe_response_api_v1_response_post**](DefaultApi.md#post_probe_response_api_v1_response_post) | **POST** /api/v1/response | Post Probe Response
[**post_probe_responses_api_v1_responses_post**](DefaultApi.md#post_probe_responses_api_v1_responses_post) | **POST** /api/v1/responses | Post Probe Responses


# **compare_sessions**
> AlignmentResults compare_sessions(session_id_1, session_id_2)

Compare Sessions

Calculate the alignment between the responses found in one sessions with those found in another session.

### Example


```python
import openapi_client
from openapi_client.models.alignment_results import AlignmentResults
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    session_id_1 = 'session_id_1_example' # str | 
    session_id_2 = 'session_id_2_example' # str | 

    try:
        # Compare Sessions
        api_response = api_instance.compare_sessions(session_id_1, session_id_2)
        print("The response of DefaultApi->compare_sessions:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->compare_sessions: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id_1** | **str**|  | 
 **session_id_2** | **str**|  | 

### Return type

[**AlignmentResults**](AlignmentResults.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **computed_kdma_profile**
> List[KDMA] computed_kdma_profile(session_id)

Compute KDMA Profile

Get KDMA profile resulting from all probe responses of the indicated session.

### Example


```python
import openapi_client
from openapi_client.models.kdma import KDMA
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    session_id = 'session_id_example' # str | 

    try:
        # Compute KDMA Profile
        api_response = api_instance.computed_kdma_profile(session_id)
        print("The response of DefaultApi->computed_kdma_profile:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->computed_kdma_profile: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**|  | 

### Return type

[**List[KDMA]**](KDMA.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_alignment_target_api_v1_alignment_target_target_id_get**
> GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response get_alignment_target_api_v1_alignment_target_target_id_get(target_id, population=population)

Get Alignment Target

Retrieve alignment target.  Parameters ---------- target_id: str     id of alignment target population: boolean     if true, treat target as distribution rather than a single set of attribute scores  Returns ---------- tgt: AlignmentTarget object     json-compatible alignment target pydantic object with schema defined in ../schema/.

### Example


```python
import openapi_client
from openapi_client.models.get_alignment_target_api_v1_alignment_target_target_id_get200_response import GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    target_id = 'alignment-target-1' # str | 
    population = False # bool |  (optional) (default to False)

    try:
        # Get Alignment Target
        api_response = api_instance.get_alignment_target_api_v1_alignment_target_target_id_get(target_id, population=population)
        print("The response of DefaultApi->get_alignment_target_api_v1_alignment_target_target_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_alignment_target_api_v1_alignment_target_target_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **target_id** | **str**|  | 
 **population** | **bool**|  | [optional] [default to False]

### Return type

[**GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response**](GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get**
> AlignmentTargetDistribution get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get(target_id)

Get Alignment Target Distribution

Retrieve alignment target distribution (which captures the alignment spread of the target population).  Note: This endpoint is optional and may not be implemented by all TA1 servers.  Parameters ---------- target_id: str     id of alignment target  Returns ---------- tgt: AlignmentTargetDistribution object     json-compatible alignment target distribution pydantic object with schema defined in ../schema/.

### Example


```python
import openapi_client
from openapi_client.models.alignment_target_distribution import AlignmentTargetDistribution
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    target_id = 'target_id_example' # str | 

    try:
        # Get Alignment Target Distribution
        api_response = api_instance.get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get(target_id)
        print("The response of DefaultApi->get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **target_id** | **str**|  | 

### Return type

[**AlignmentTargetDistribution**](AlignmentTargetDistribution.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get**
> List[str] get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get()

Get alignment targets distribution IDs

Retrieve an array of alignment targets distribution IDs.  Each ID is a valid identifier for an AlignmentTargetDistribution.

### Example


```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        # Get alignment targets distribution IDs
        api_response = api_instance.get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get()
        print("The response of DefaultApi->get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**List[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_alignment_target_ids_api_v1_alignment_target_ids_get**
> List[str] get_alignment_target_ids_api_v1_alignment_target_ids_get()

Get alignment targets IDs

Retrieve an array of alignment targets IDs.  Each ID is a valid identifier for an AlignmentTarget.

### Example


```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        # Get alignment targets IDs
        api_response = api_instance.get_alignment_target_ids_api_v1_alignment_target_ids_get()
        print("The response of DefaultApi->get_alignment_target_ids_api_v1_alignment_target_ids_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_alignment_target_ids_api_v1_alignment_target_ids_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**List[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_api_api_v1_get**
> object get_api_api_v1_get()

Get Api

Return API version, can be used to check connection.  Parameters ----------- None  Returns -------- version: dict     key (str): 'api_version', value (str): version string

### Example


```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        # Get Api
        api_response = api_instance.get_api_api_v1_get()
        print("The response of DefaultApi->get_api_api_v1_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_api_api_v1_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_most_and_least_aligned**
> GetMostAndLeastAligned200Response get_most_and_least_aligned(session_id)

Get Most and Least Aligned Targets

Get IDs for the alignment targets that responses in the indicated sessions are least and most aligned with.

### Example


```python
import openapi_client
from openapi_client.models.get_most_and_least_aligned200_response import GetMostAndLeastAligned200Response
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    session_id = 'session_id_example' # str | 

    try:
        # Get Most and Least Aligned Targets
        api_response = api_instance.get_most_and_least_aligned(session_id)
        print("The response of DefaultApi->get_most_and_least_aligned:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_most_and_least_aligned: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**|  | 

### Return type

[**GetMostAndLeastAligned200Response**](GetMostAndLeastAligned200Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_probe_response_alignment_api_v1_alignment_probe_get**
> AlignmentResults get_probe_response_alignment_api_v1_alignment_probe_get(session_id, target_id, scenario_id, probe_id, population=population)

Get Probe Response Alignment

Get probe-level alignment

### Example


```python
import openapi_client
from openapi_client.models.alignment_results import AlignmentResults
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    session_id = 'session_id_example' # str | 
    target_id = 'alignment-target-1' # str | 
    scenario_id = 'scenario-1' # str | 
    probe_id = 'probe-1' # str | 
    population = False # bool |  (optional) (default to False)

    try:
        # Get Probe Response Alignment
        api_response = api_instance.get_probe_response_alignment_api_v1_alignment_probe_get(session_id, target_id, scenario_id, probe_id, population=population)
        print("The response of DefaultApi->get_probe_response_alignment_api_v1_alignment_probe_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_probe_response_alignment_api_v1_alignment_probe_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**|  | 
 **target_id** | **str**|  | 
 **scenario_id** | **str**|  | 
 **probe_id** | **str**|  | 
 **population** | **bool**|  | [optional] [default to False]

### Return type

[**AlignmentResults**](AlignmentResults.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scenario_api_v1_scenario_scenario_id_get**
> Scenario get_scenario_api_v1_scenario_scenario_id_get(scenario_id)

Get Scenario

Retrieve scenario.  Parameters ----------- scenario_id: str, required     id of scenario  Returns ------------ scenario: Scenario object     json-compatible scenario pydantic object with schema defined in ../schema/.

### Example


```python
import openapi_client
from openapi_client.models.scenario import Scenario
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    scenario_id = 'scenario-1' # str | 

    try:
        # Get Scenario
        api_response = api_instance.get_scenario_api_v1_scenario_scenario_id_get(scenario_id)
        print("The response of DefaultApi->get_scenario_api_v1_scenario_scenario_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_scenario_api_v1_scenario_scenario_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scenario_id** | **str**|  | 

### Return type

[**Scenario**](Scenario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_session_alignment_api_v1_alignment_session_get**
> AlignmentResults get_session_alignment_api_v1_alignment_session_get(session_id, target_id, population=population)

Get Session Alignment

Get alignment for all probe responses in a session.

### Example


```python
import openapi_client
from openapi_client.models.alignment_results import AlignmentResults
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    session_id = 'session_id_example' # str | 
    target_id = 'alignment-target-1' # str | 
    population = False # bool |  (optional) (default to False)

    try:
        # Get Session Alignment
        api_response = api_instance.get_session_alignment_api_v1_alignment_session_get(session_id, target_id, population=population)
        print("The response of DefaultApi->get_session_alignment_api_v1_alignment_session_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get_session_alignment_api_v1_alignment_session_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**|  | 
 **target_id** | **str**|  | 
 **population** | **bool**|  | [optional] [default to False]

### Return type

[**AlignmentResults**](AlignmentResults.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_new_session_id_api_v1_new_session_post**
> str post_new_session_id_api_v1_new_session_post()

Post New Session Id

Get unique session id for grouping answers from a collection of scenarios/probes together when computing kdma values/alignment results.

### Example


```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        # Post New Session Id
        api_response = api_instance.post_new_session_id_api_v1_new_session_post()
        print("The response of DefaultApi->post_new_session_id_api_v1_new_session_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->post_new_session_id_api_v1_new_session_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_probe_response_api_v1_response_post**
> object post_probe_response_api_v1_response_post(probe_response)

Post Probe Response

Send probe response to be stored in database.  Parameters ---------- response: ProbeResponse object (defined in pydantic schema)     Contains session id, probe id, response id  Returns ----------- None

### Example


```python
import openapi_client
from openapi_client.models.probe_response import ProbeResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    probe_response = openapi_client.ProbeResponse() # ProbeResponse | 

    try:
        # Post Probe Response
        api_response = api_instance.post_probe_response_api_v1_response_post(probe_response)
        print("The response of DefaultApi->post_probe_response_api_v1_response_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->post_probe_response_api_v1_response_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **probe_response** | [**ProbeResponse**](ProbeResponse.md)|  | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_probe_responses_api_v1_responses_post**
> object post_probe_responses_api_v1_responses_post(probe_response_batch)

Post Probe Responses

Send collection of probe responses to be stored in database.  Parameters ---------- session_id: str     unique id for user session probe_ids: List[str]:     list of id's corresponding to probes response_ids: List[str]     list of id's corresponding to responses  Returns ----------- None

### Example


```python
import openapi_client
from openapi_client.models.probe_response_batch import ProbeResponseBatch
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    probe_response_batch = openapi_client.ProbeResponseBatch() # ProbeResponseBatch | 

    try:
        # Post Probe Responses
        api_response = api_instance.post_probe_responses_api_v1_responses_post(probe_response_batch)
        print("The response of DefaultApi->post_probe_responses_api_v1_responses_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->post_probe_responses_api_v1_responses_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **probe_response_batch** | [**ProbeResponseBatch**](ProbeResponseBatch.md)|  | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

