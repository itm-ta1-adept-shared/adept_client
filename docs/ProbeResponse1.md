# ProbeResponse1

encapsulates the selection by a DM of an option in response to a probe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scenario_id** | **str** | globally unique scenario ID | 
**probe_id** | **str** | globally unique probe ID | 
**choice** | **str** | id of choice made | 
**justification** | **str** | A justification of the response to the probe | [optional] 

## Example

```python
from openapi_client.models.probe_response1 import ProbeResponse1

# TODO update the JSON string below
json = "{}"
# create an instance of ProbeResponse1 from a JSON string
probe_response1_instance = ProbeResponse1.from_json(json)
# print the JSON string representation of the object
print(ProbeResponse1.to_json())

# convert the object into a dict
probe_response1_dict = probe_response1_instance.to_dict()
# create an instance of ProbeResponse1 from a dict
probe_response1_from_dict = ProbeResponse1.from_dict(probe_response1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


