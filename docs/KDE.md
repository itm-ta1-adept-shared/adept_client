# KDE


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kde** | **str** | Serialized KDE object as a base64 string | [optional] 

## Example

```python
from openapi_client.models.kde import KDE

# TODO update the JSON string below
json = "{}"
# create an instance of KDE from a JSON string
kde_instance = KDE.from_json(json)
# print the JSON string representation of the object
print(KDE.to_json())

# convert the object into a dict
kde_dict = kde_instance.to_dict()
# create an instance of KDE from a dict
kde_from_dict = KDE.from_dict(kde_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


