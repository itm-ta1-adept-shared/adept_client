# MedicalPoliciesEnum

Directives issued by competent military authority

## Enum

* `TREAT_ALL_NEUTRALLY` (value: `'Treat All Neutrally'`)

* `TREAT_ENEMY_LLE` (value: `'Treat Enemy LLE'`)

* `TREAT_CIVILIAN_LLE` (value: `'Treat Civilian LLE'`)

* `PRIORITIZE_MISSION` (value: `'Prioritize Mission'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


