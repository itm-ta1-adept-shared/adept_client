# FaunaTypeEnum

Descriptor of local animal/insect activity

## Enum

* `NONE` (value: `'none'`)

* `LIMITED` (value: `'limited'`)

* `NORMAL` (value: `'normal'`)

* `HIGH` (value: `'high'`)

* `PERVASIVE` (value: `'pervasive'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


