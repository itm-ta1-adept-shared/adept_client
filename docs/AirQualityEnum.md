# AirQualityEnum

Air Quality Index (AQI); see [airnow.gov](https://www.airnow.gov/aqi/aqi-basics/)

## Enum

* `GREEN` (value: `'green'`)

* `YELLOW` (value: `'yellow'`)

* `ORANGE` (value: `'orange'`)

* `RED` (value: `'red'`)

* `PURPLE` (value: `'purple'`)

* `MAROON` (value: `'maroon'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


