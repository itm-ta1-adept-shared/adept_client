# VisibilityTypeEnum

Descriptor for operational visibility; affected by time of day, lighting, weather, terrain, etc.

## Enum

* `NONE` (value: `'none'`)

* `VERY_LOW` (value: `'very low'`)

* `LOW` (value: `'low'`)

* `MODERATE` (value: `'moderate'`)

* `GOOD` (value: `'good'`)

* `EXCELLENT` (value: `'excellent'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


