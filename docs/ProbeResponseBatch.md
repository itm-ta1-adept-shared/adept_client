# ProbeResponseBatch

Response to multiple probes sent together.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**session_id** | **str** | Unique ID for user session. | 
**responses** | [**List[Response]**](Response.md) | string IDs of scenario that response is for | 

## Example

```python
from openapi_client.models.probe_response_batch import ProbeResponseBatch

# TODO update the JSON string below
json = "{}"
# create an instance of ProbeResponseBatch from a JSON string
probe_response_batch_instance = ProbeResponseBatch.from_json(json)
# print the JSON string representation of the object
print(ProbeResponseBatch.to_json())

# convert the object into a dict
probe_response_batch_dict = probe_response_batch_instance.to_dict()
# create an instance of ProbeResponseBatch from a dict
probe_response_batch_from_dict = ProbeResponseBatch.from_dict(probe_response_batch_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


