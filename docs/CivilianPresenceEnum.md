# CivilianPresenceEnum

Indicator of how many civilians are present in the mission

## Enum

* `NONE` (value: `'none'`)

* `LIMITED` (value: `'limited'`)

* `SOME` (value: `'some'`)

* `EXTENSIVE` (value: `'extensive'`)

* `CROWD` (value: `'crowd'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


