# GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Globally unique ID for profile | 
**kdma_values** | [**List[KDMA]**](KDMA.md) |  | 
**population** | [**List[AlignmentTarget]**](AlignmentTarget.md) |  | 

## Example

```python
from openapi_client.models.get_alignment_target_api_v1_alignment_target_target_id_get200_response import GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response

# TODO update the JSON string below
json = "{}"
# create an instance of GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response from a JSON string
get_alignment_target_api_v1_alignment_target_target_id_get200_response_instance = GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response.from_json(json)
# print the JSON string representation of the object
print(GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response.to_json())

# convert the object into a dict
get_alignment_target_api_v1_alignment_target_target_id_get200_response_dict = get_alignment_target_api_v1_alignment_target_target_id_get200_response_instance.to_dict()
# create an instance of GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response from a dict
get_alignment_target_api_v1_alignment_target_target_id_get200_response_from_dict = GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response.from_dict(get_alignment_target_api_v1_alignment_target_target_id_get200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


