# PopulationDensityEnum

persons per square meter, each successive term is one more person per square meter

## Enum

* `NONE` (value: `'none'`)

* `SPARSE` (value: `'sparse'`)

* `SOME` (value: `'some'`)

* `BUSY` (value: `'busy'`)

* `CROWDED` (value: `'crowded'`)

* `VERY_CROWDED` (value: `'very crowded'`)

* `EXTREME` (value: `'extreme'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


