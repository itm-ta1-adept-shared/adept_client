# MissionTypeEnum

enumeration of possible mission types

## Enum

* `ATTACK` (value: `'Attack'`)

* `DEFEND` (value: `'Defend'`)

* `DELAY` (value: `'Delay'`)

* `PATROL` (value: `'Patrol'`)

* `RECONNAISSANCE` (value: `'Reconnaissance'`)

* `AMBUSH` (value: `'Ambush'`)

* `LISTENING_SLASH_OBSERVATION` (value: `'Listening/Observation'`)

* `DIRECT_ACTION` (value: `'Direct Action'`)

* `HOSTAGE_RESCUE` (value: `'Hostage rescue'`)

* `ASSET_TRANSPORT` (value: `'Asset transport'`)

* `SENSOR_EMPLACEMENT` (value: `'Sensor emplacement'`)

* `INTELLIGENCE_GATHERING` (value: `'Intelligence gathering'`)

* `CIVIL_AFFAIRS` (value: `'Civil affairs'`)

* `TRAINING` (value: `'Training'`)

* `SABOTAGE` (value: `'Sabotage'`)

* `SECURITY_PATROL` (value: `'Security patrol'`)

* `FIRE_SUPPORT` (value: `'Fire support'`)

* `NUCLEAR_DETERRENCE` (value: `'Nuclear deterrence'`)

* `EXTRACTION` (value: `'Extraction'`)

* `UNKNOWN` (value: `'Unknown'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


