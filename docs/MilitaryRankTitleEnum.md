# MilitaryRankTitleEnum

the branch-specific military rank

## Enum

* `PRIVATE_LEFT_PARENTHESIS_RECRUIT_RIGHT_PARENTHESIS` (value: `'Private (Recruit)'`)

* `PRIVATE` (value: `'Private'`)

* `PRIVATE_FIRST_CLASS` (value: `'Private First Class'`)

* `SPECIALIST` (value: `'Specialist'`)

* `CORPORAL` (value: `'Corporal'`)

* `SERGEANT` (value: `'Sergeant'`)

* `STAFF_SERGEANT` (value: `'Staff Sergeant'`)

* `SERGEANT_FIRST_CLASS` (value: `'Sergeant First Class'`)

* `MASTER_SERGEANT` (value: `'Master Sergeant'`)

* `FIRST_SERGEANT` (value: `'First Sergeant'`)

* `SERGEANT_MAJOR` (value: `'Sergeant Major'`)

* `COMMAND_SERGEANT_MAJOR` (value: `'Command Sergeant Major'`)

* `SERGEANT_MAJOR_OF_THE_ARMY` (value: `'Sergeant Major of the Army'`)

* `WARRANT_OFFICER_1` (value: `'Warrant Officer 1'`)

* `CHIEF_WARRANT_OFFICER_2` (value: `'Chief Warrant Officer 2'`)

* `CHIEF_WARRANT_OFFICER_3` (value: `'Chief Warrant Officer 3'`)

* `CHIEF_WARRANT_OFFICER_4` (value: `'Chief Warrant Officer 4'`)

* `CHIEF_WARRANT_OFFICER_5` (value: `'Chief Warrant Officer 5'`)

* `ENUM_2ND_LIEUTENANT` (value: `'2nd Lieutenant'`)

* `ENUM_1ST_LIEUTENANT` (value: `'1st Lieutenant'`)

* `LIEUTENANT` (value: `'Lieutenant'`)

* `CAPTAIN` (value: `'Captain'`)

* `MAJOR` (value: `'Major'`)

* `LIEUTENANT_COLONEL` (value: `'Lieutenant Colonel'`)

* `COLONEL` (value: `'Colonel'`)

* `BRIGADIER_GENERAL` (value: `'Brigadier General'`)

* `MAJOR_GENERAL` (value: `'Major General'`)

* `LIEUTENANT_GENERAL` (value: `'Lieutenant General'`)

* `ARMY_CHIEF_OF_STAFF_LEFT_PARENTHESIS_SPECIAL_RIGHT_PARENTHESIS` (value: `'Army Chief of Staff (special)'`)

* `GENERAL` (value: `'General'`)

* `AIRMAN_BASIC` (value: `'Airman Basic'`)

* `AIRMAN` (value: `'Airman'`)

* `AIRMAN_FIRST_CLASS` (value: `'Airman First Class'`)

* `SENIOR_AIRMAN` (value: `'Senior Airman'`)

* `TECHNICAL_SERGEANT` (value: `'Technical Sergeant'`)

* `SENIOR_MASTER_SERGEANT` (value: `'Senior Master Sergeant'`)

* `FIRST_SERGEANT_SLASH__CHIEF_MASTER_SERGEANT` (value: `'First Sergeant / Chief Master Sergeant'`)

* `CHIEF_MASTER_SERGEANT_OF_THE_AIR_FORCE` (value: `'Chief Master Sergeant of the Air Force'`)

* `AIR_FORCE_CHIEF_OF_STAFF_LEFT_PARENTHESIS_SPECIAL_RIGHT_PARENTHESIS` (value: `'Air Force Chief of Staff (special)'`)

* `SEAMAN_RECRUIT` (value: `'Seaman Recruit'`)

* `SEAMAN_APPRENTICE` (value: `'Seaman Apprentice'`)

* `SEAMAN` (value: `'Seaman'`)

* `PETTY_OFFICER_THIRD_CLASS` (value: `'Petty Officer Third Class'`)

* `PETTY_OFFICER_SECOND_CLASS` (value: `'Petty Officer Second Class'`)

* `PETTY_OFFICER_FIRST_CLASS` (value: `'Petty Officer First Class'`)

* `CHIEF_PETTY_OFFICER` (value: `'Chief Petty Officer'`)

* `SENIOR_CHIEF_PETTY_OFFICER` (value: `'Senior Chief Petty Officer'`)

* `MASTER_CHIEF_PETTY_OFFICER` (value: `'Master Chief Petty Officer'`)

* `MASTER_CHIEF_PETTY_OFFICER_OF_THE_NAVY` (value: `'Master Chief Petty Officer of the Navy'`)

* `MASTER_CHIEF_PETTY_OFFICER_OF_THE_COAST_GUARD` (value: `'Master Chief Petty Officer of the Coast Guard'`)

* `CHIEF_WARRANT_OFFICER` (value: `'Chief Warrant Officer'`)

* `ENSIGN` (value: `'Ensign'`)

* `LIEUTENANT_COMMA__JUNIOR_GRADE` (value: `'Lieutenant, Junior Grade'`)

* `LIEUTENANT_COMMANDER` (value: `'Lieutenant Commander'`)

* `COMMANDER` (value: `'Commander'`)

* `REAR_ADMIRAL_LEFT_PARENTHESIS_LOWER_HALF_RIGHT_PARENTHESIS` (value: `'Rear Admiral (Lower Half)'`)

* `REAR_ADMIRAL_LEFT_PARENTHESIS_UPPER_HALF_RIGHT_PARENTHESIS` (value: `'Rear Admiral (Upper Half)'`)

* `VICE_ADMIRAL` (value: `'Vice Admiral'`)

* `CHIEF_OF_NAVAL_OPERATIONS_LEFT_PARENTHESIS_SPECIAL_RIGHT_PARENTHESIS` (value: `'Chief of Naval Operations (special)'`)

* `COMMANDANT_OF_THE_COAST_GUARD_LEFT_PARENTHESIS_SPECIAL_RIGHT_PARENTHESIS` (value: `'Commandant of the Coast Guard (special)'`)

* `ADMIRAL` (value: `'Admiral'`)

* `LANCE_CORPORAL` (value: `'Lance Corporal'`)

* `GUNNERY_SERGEANT` (value: `'Gunnery Sergeant'`)

* `MASTER_GUNNERY_SERGEANT` (value: `'Master Gunnery Sergeant'`)

* `SERGEANT_MAJOR_OF_THE_MARINE_CORPS` (value: `'Sergeant Major of the Marine Corps'`)

* `WARRANT_OFFICER` (value: `'Warrant Officer'`)

* `COMMANDANT_OF_THE_MARINE_CORPS` (value: `'Commandant of the Marine Corps'`)

* `SPECIALIST_1` (value: `'Specialist 1'`)

* `SPECIALIST_2` (value: `'Specialist 2'`)

* `SPECIALIST_3` (value: `'Specialist 3'`)

* `SPECIALIST_4` (value: `'Specialist 4'`)

* `CHIEF_MASTER_SERGEANT` (value: `'Chief Master Sergeant'`)

* `CHIEF_MASTER_SERGEANT_OF_THE_SPACE_FORCE` (value: `'Chief Master Sergeant of the Space Force'`)

* `CHIEF_OF_SPACE_OPERATIONS` (value: `'Chief of Space Operations'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


