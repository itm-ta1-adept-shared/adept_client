# KDMAName

Possible KDMA names.

## Enum

* `DENIAL` (value: `'denial'`)

* `MISSION` (value: `'mission'`)

* `BASICKNOWLEDGE` (value: `'BasicKnowledge'`)

* `TIMEPRESSURE` (value: `'TimePressure'`)

* `RISKAVERSION` (value: `'RiskAversion'`)

* `PROTOCOLFOCUS` (value: `'ProtocolFocus'`)

* `FAIRNESS` (value: `'Fairness'`)

* `UTILITARIANISM` (value: `'Utilitarianism'`)

* `MORALDESERT` (value: `'MoralDesert'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


