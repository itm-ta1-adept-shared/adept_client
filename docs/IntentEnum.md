# IntentEnum

The intent of the character

## Enum

* `INTEND_MAJOR_HARM` (value: `'intend major harm'`)

* `INTEND_MINOR_HARM` (value: `'intend minor harm'`)

* `NO_INTENT` (value: `'no intent'`)

* `INTEND_MINOR_HELP` (value: `'intend minor help'`)

* `INTEND_MAJOR_HELP` (value: `'intend major help'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


