# DirectnessEnum

How directly a character is responsible for injury

## Enum

* `DIRECT` (value: `'direct'`)

* `SOMEWHAT_DIRECT` (value: `'somewhat direct'`)

* `SOMEWHAT_INDIRECT` (value: `'somewhat indirect'`)

* `INDIRECT` (value: `'indirect'`)

* `NONE` (value: `'none'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


