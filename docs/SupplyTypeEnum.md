# SupplyTypeEnum

an enumeration of available supply types

## Enum

* `TOURNIQUET` (value: `'Tourniquet'`)

* `PRESSURE_BANDAGE` (value: `'Pressure bandage'`)

* `HEMOSTATIC_GAUZE` (value: `'Hemostatic gauze'`)

* `DECOMPRESSION_NEEDLE` (value: `'Decompression Needle'`)

* `NASOPHARYNGEAL_AIRWAY` (value: `'Nasopharyngeal airway'`)

* `PULSE_OXIMETER` (value: `'Pulse Oximeter'`)

* `BLANKET` (value: `'Blanket'`)

* `EPI_PEN` (value: `'Epi Pen'`)

* `VENTED_CHEST_SEAL` (value: `'Vented Chest Seal'`)

* `PAIN_MEDICATIONS` (value: `'Pain Medications'`)

* `FENTANYL_LOLLIPOP` (value: `'Fentanyl Lollipop'`)

* `SPLINT` (value: `'Splint'`)

* `BLOOD` (value: `'Blood'`)

* `IV_BAG` (value: `'IV Bag'`)

* `BURN_DRESSING` (value: `'Burn Dressing'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


