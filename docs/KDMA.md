# KDMA

Single KDMA value with values between 0 and 1, or (not used) a kernel density estimate of the KDMA value.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kdma** | [**KDMAName**](KDMAName.md) |  | 
**value** | **float** |  | [optional] 
**kdes** | [**KDMAKdes**](KDMAKdes.md) |  | [optional] 

## Example

```python
from openapi_client.models.kdma import KDMA

# TODO update the JSON string below
json = "{}"
# create an instance of KDMA from a JSON string
kdma_instance = KDMA.from_json(json)
# print the JSON string representation of the object
print(KDMA.to_json())

# convert the object into a dict
kdma_dict = kdma_instance.to_dict()
# create an instance of KDMA from a dict
kdma_from_dict = KDMA.from_dict(kdma_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


