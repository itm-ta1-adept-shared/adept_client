# SimEnvironmentTypeEnum

Basic setting for the entire scenario

## Enum

* `JUNGLE` (value: `'jungle'`)

* `SUBMARINE` (value: `'submarine'`)

* `URBAN` (value: `'urban'`)

* `DESERT` (value: `'desert'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


