# TerrainTypeEnum

Descriptor for the scenario terrain

## Enum

* `JUNGLE` (value: `'jungle'`)

* `INDOORS` (value: `'indoors'`)

* `URBAN` (value: `'urban'`)

* `DUNES` (value: `'dunes'`)

* `FOREST` (value: `'forest'`)

* `BEACH` (value: `'beach'`)

* `MOUNTAIN` (value: `'mountain'`)

* `PLAINS` (value: `'plains'`)

* `HILLS` (value: `'hills'`)

* `SWAMP` (value: `'swamp'`)

* `FLAT` (value: `'flat'`)

* `ROUGH` (value: `'rough'`)

* `EXTREME` (value: `'extreme'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


