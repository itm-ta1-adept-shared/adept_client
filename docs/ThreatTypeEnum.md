# ThreatTypeEnum

the type or nature of the threat

## Enum

* `CIVIL_UNREST` (value: `'Civil unrest'`)

* `DRONE_ACTIVITY` (value: `'Drone activity'`)

* `EXTREME_WEATHER` (value: `'Extreme weather'`)

* `FIRE` (value: `'Fire'`)

* `GUNFIRE` (value: `'Gunfire'`)

* `IED_ACTIVITY` (value: `'IED activity'`)

* `MINES` (value: `'Mines'`)

* `POISONOUS_VEGETATION` (value: `'Poisonous vegetation'`)

* `PREDATORS` (value: `'Predators'`)

* `UNKNOWN` (value: `'Unknown'`)

* `UNSTABLE_STRUCTURE` (value: `'Unstable structure'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


