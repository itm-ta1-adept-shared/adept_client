# RapportEnum

A measure of closeness or affinity towards the player/medic

## Enum

* `LOATHING` (value: `'loathing'`)

* `DISLIKE` (value: `'dislike'`)

* `NEUTRAL` (value: `'neutral'`)

* `CLOSE` (value: `'close'`)

* `FAMILIAL` (value: `'familial'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


