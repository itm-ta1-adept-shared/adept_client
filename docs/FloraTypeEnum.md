# FloraTypeEnum

Descriptor of local vegetation.

## Enum

* `NONE` (value: `'none'`)

* `LIMITED` (value: `'limited'`)

* `NORMAL` (value: `'normal'`)

* `LUSH` (value: `'lush'`)

* `EXTENSIVE` (value: `'extensive'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


