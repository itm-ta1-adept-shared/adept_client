# SkillTypeEnum

the type of skill the character has

## Enum

* `MEDICAL` (value: `'Medical'`)

* `COMBAT` (value: `'Combat'`)

* `SPECIALIST` (value: `'Specialist'`)

* `COMMUNICATIONS` (value: `'Communications'`)

* `COMMAND` (value: `'Command'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


