# PeakNoiseEnum

Descriptor for peak noise level due to gunfire, vehicles, helicopters, etc.

## Enum

* `NONE` (value: `'none'`)

* `QUIET` (value: `'quiet'`)

* `NORMAL` (value: `'normal'`)

* `NOISY` (value: `'noisy'`)

* `EXTREME` (value: `'extreme'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


