# BreathingLevelEnum

Descriptive breathing level

## Enum

* `NORMAL` (value: `'NORMAL'`)

* `FAST` (value: `'FAST'`)

* `SLOW` (value: `'SLOW'`)

* `RESTRICTED` (value: `'RESTRICTED'`)

* `NONE` (value: `'NONE'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


