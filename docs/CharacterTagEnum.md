# CharacterTagEnum

the tag assigned to a character

## Enum

* `MINIMAL` (value: `'MINIMAL'`)

* `DELAYED` (value: `'DELAYED'`)

* `IMMEDIATE` (value: `'IMMEDIATE'`)

* `EXPECTANT` (value: `'EXPECTANT'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


