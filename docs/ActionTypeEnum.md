# ActionTypeEnum

An action type [recognized by the ADM Server](https://github.com/NextCenturyCorporation/itm-evaluation-client?tab=readme-ov-file#available-actions)

## Enum

* `APPLY_TREATMENT` (value: `'APPLY_TREATMENT'`)

* `CHECK_ALL_VITALS` (value: `'CHECK_ALL_VITALS'`)

* `CHECK_BLOOD_OXYGEN` (value: `'CHECK_BLOOD_OXYGEN'`)

* `CHECK_PULSE` (value: `'CHECK_PULSE'`)

* `CHECK_RESPIRATION` (value: `'CHECK_RESPIRATION'`)

* `DIRECT_MOBILE_CHARACTERS` (value: `'DIRECT_MOBILE_CHARACTERS'`)

* `END_SCENE` (value: `'END_SCENE'`)

* `MESSAGE` (value: `'MESSAGE'`)

* `MOVE_TO` (value: `'MOVE_TO'`)

* `MOVE_TO_EVAC` (value: `'MOVE_TO_EVAC'`)

* `SEARCH` (value: `'SEARCH'`)

* `SITREP` (value: `'SITREP'`)

* `TAG_CHARACTER` (value: `'TAG_CHARACTER'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


