# OxygenLevelsEnum

Oxygen levels due to any factor that may impact decision-making

## Enum

* `NORMAL` (value: `'normal'`)

* `LIMITED` (value: `'limited'`)

* `SCARCE` (value: `'scarce'`)

* `NONE` (value: `'none'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


