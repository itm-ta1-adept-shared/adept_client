# AlignmentTarget

Desired profile of KDMA values for an algorithmic decision maker to align to.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Globally unique ID for profile | 
**kdma_values** | [**List[KDMA]**](KDMA.md) |  | 

## Example

```python
from openapi_client.models.alignment_target import AlignmentTarget

# TODO update the JSON string below
json = "{}"
# create an instance of AlignmentTarget from a JSON string
alignment_target_instance = AlignmentTarget.from_json(json)
# print the JSON string representation of the object
print(AlignmentTarget.to_json())

# convert the object into a dict
alignment_target_dict = alignment_target_instance.to_dict()
# create an instance of AlignmentTarget from a dict
alignment_target_from_dict = AlignmentTarget.from_dict(alignment_target_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


