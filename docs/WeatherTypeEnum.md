# WeatherTypeEnum

Descriptor of the scenario weather

## Enum

* `CLEAR` (value: `'clear'`)

* `WIND` (value: `'wind'`)

* `CLOUDS` (value: `'clouds'`)

* `RAIN` (value: `'rain'`)

* `FOG` (value: `'fog'`)

* `THUNDERSTORM` (value: `'thunderstorm'`)

* `HAIL` (value: `'hail'`)

* `SLEET` (value: `'sleet'`)

* `SNOW` (value: `'snow'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


