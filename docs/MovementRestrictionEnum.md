# MovementRestrictionEnum

Operational movement restrictions due to any factor including terrain, weather, enemy activity, etc.

## Enum

* `UNRESTRICTED` (value: `'unrestricted'`)

* `MINIMAL` (value: `'minimal'`)

* `MODERATE` (value: `'moderate'`)

* `SEVERE` (value: `'severe'`)

* `EXTREME` (value: `'extreme'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


