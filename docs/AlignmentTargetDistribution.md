# AlignmentTargetDistribution

All KDMA values for each member of the target population for an algorithmic decision maker to align to.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Globally unique ID for profile | 
**population** | [**List[AlignmentTarget]**](AlignmentTarget.md) |  | 

## Example

```python
from openapi_client.models.alignment_target_distribution import AlignmentTargetDistribution

# TODO update the JSON string below
json = "{}"
# create an instance of AlignmentTargetDistribution from a JSON string
alignment_target_distribution_instance = AlignmentTargetDistribution.from_json(json)
# print the JSON string representation of the object
print(AlignmentTargetDistribution.to_json())

# convert the object into a dict
alignment_target_distribution_dict = alignment_target_distribution_instance.to_dict()
# create an instance of AlignmentTargetDistribution from a dict
alignment_target_distribution_from_dict = AlignmentTargetDistribution.from_dict(alignment_target_distribution_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


