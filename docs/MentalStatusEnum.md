# MentalStatusEnum

Character mental status, which impacts interaction in the sim environment

## Enum

* `AGONY` (value: `'AGONY'`)

* `CALM` (value: `'CALM'`)

* `CONFUSED` (value: `'CONFUSED'`)

* `SHOCK` (value: `'SHOCK'`)

* `UPSET` (value: `'UPSET'`)

* `UNRESPONSIVE` (value: `'UNRESPONSIVE'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


