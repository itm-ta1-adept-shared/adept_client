# CharacterRoleEnum

The primary role a character has in the mission, in terms of the skills they possess

## Enum

* `INFANTRY` (value: `'Infantry'`)

* `SEAL` (value: `'SEAL'`)

* `COMMAND` (value: `'Command'`)

* `INTELLIGENCE` (value: `'Intelligence'`)

* `MEDICAL` (value: `'Medical'`)

* `SPECIALIST` (value: `'Specialist'`)

* `COMMUNICATIONS` (value: `'Communications'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


