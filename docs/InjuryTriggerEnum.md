# InjuryTriggerEnum

What source caused character injuries

## Enum

* `EXPLOSION` (value: `'explosion'`)

* `FIREARM` (value: `'firearm'`)

* `FALL` (value: `'fall'`)

* `FIGHT` (value: `'fight'`)

* `PATHOGEN` (value: `'pathogen'`)

* `POISON` (value: `'poison'`)

* `ANIMAL` (value: `'animal'`)

* `PLANT` (value: `'plant'`)

* `WATER` (value: `'water'`)

* `COLLISION` (value: `'collision'`)

* `ELECTRICAL` (value: `'electrical'`)

* `EQUIPMENT` (value: `'equipment'`)

* `ATTACK` (value: `'attack'`)

* `FIRE` (value: `'fire'`)

* `STRESS` (value: `'stress'`)

* `CHEMICAL` (value: `'chemical'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


