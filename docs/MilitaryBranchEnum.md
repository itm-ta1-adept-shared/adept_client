# MilitaryBranchEnum

Branch of the US military.

## Enum

* `US_ARMY` (value: `'US Army'`)

* `US_NAVY` (value: `'US Navy'`)

* `US_AIR_FORCE` (value: `'US Air Force'`)

* `US_MARINE_CORPS` (value: `'US Marine Corps'`)

* `US_SPACE_FORCE` (value: `'US Space Force'`)

* `US_COAST_GUARD` (value: `'US Coast Guard'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


