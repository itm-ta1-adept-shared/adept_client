# MissionImportanceEnum

How important the character is to the mission

## Enum

* `LOW` (value: `'low'`)

* `NORMAL` (value: `'normal'`)

* `IMPORTANT` (value: `'important'`)

* `PRIORITY` (value: `'priority'`)

* `VIP` (value: `'vip'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


