# KDMAKdes


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rawscores** | [**KDE**](KDE.md) |  | [optional] 
**globalnorm** | [**KDE**](KDE.md) |  | [optional] 
**localnorm** | [**KDE**](KDE.md) |  | [optional] 
**globalnormx_localnormy** | [**KDE**](KDE.md) |  | [optional] 

## Example

```python
from openapi_client.models.kdma_kdes import KDMAKdes

# TODO update the JSON string below
json = "{}"
# create an instance of KDMAKdes from a JSON string
kdma_kdes_instance = KDMAKdes.from_json(json)
# print the JSON string representation of the object
print(KDMAKdes.to_json())

# convert the object into a dict
kdma_kdes_dict = kdma_kdes_instance.to_dict()
# create an instance of KDMAKdes from a dict
kdma_kdes_from_dict = KDMAKdes.from_dict(kdma_kdes_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


