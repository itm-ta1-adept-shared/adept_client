# BloodOxygenEnum

A description of the percentage of oxygen in someone's blood, as measured by a pulse oximeter

## Enum

* `NORMAL` (value: `'NORMAL'`)

* `LOW` (value: `'LOW'`)

* `NONE` (value: `'NONE'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


