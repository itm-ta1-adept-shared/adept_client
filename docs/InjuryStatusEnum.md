# InjuryStatusEnum

Whether the injury is known prior- and post-assessment, and to what extent it's been treated

## Enum

* `HIDDEN` (value: `'hidden'`)

* `DISCOVERABLE` (value: `'discoverable'`)

* `VISIBLE` (value: `'visible'`)

* `DISCOVERED` (value: `'discovered'`)

* `PARTIALLY_TREATED` (value: `'partially treated'`)

* `TREATED` (value: `'treated'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


