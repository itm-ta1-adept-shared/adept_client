# LightingTypeEnum

Descriptor of available natural or man-made lighting

## Enum

* `NONE` (value: `'none'`)

* `LIMITED` (value: `'limited'`)

* `NORMAL` (value: `'normal'`)

* `BRIGHT` (value: `'bright'`)

* `FLASHING` (value: `'flashing'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


