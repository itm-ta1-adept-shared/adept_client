# AvpuLevelEnum

Character level of response; anything but ALERT is considered unconscious.  See [Levels of Response](https://www.firstresponse.org.uk/first-aid-az/3-general/first-aid/79-levels-of-response) for details

## Enum

* `ALERT` (value: `'ALERT'`)

* `VOICE` (value: `'VOICE'`)

* `PAIN` (value: `'PAIN'`)

* `UNRESPONSIVE` (value: `'UNRESPONSIVE'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


