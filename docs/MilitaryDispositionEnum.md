# MilitaryDispositionEnum

How the character is to be treated in a military context

## Enum

* `ALLIED_US` (value: `'Allied US'`)

* `ALLIED` (value: `'Allied'`)

* `CIVILIAN` (value: `'Civilian'`)

* `MILITARY_ADVERSARY` (value: `'Military Adversary'`)

* `MILITARY_NEUTRAL` (value: `'Military Neutral'`)

* `NON_MINUS_MILITARY_ADVERSARY` (value: `'Non-Military Adversary'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


