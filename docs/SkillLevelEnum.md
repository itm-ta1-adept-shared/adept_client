# SkillLevelEnum

the level of expertise the character has in the skill

## Enum

* `NOVICE` (value: `'novice'`)

* `QUALIFIED` (value: `'qualified'`)

* `COMPETENT` (value: `'competent'`)

* `SKILLED` (value: `'skilled'`)

* `EXPERT` (value: `'expert'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


