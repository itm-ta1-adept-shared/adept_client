# MilitaryRankEnum

the cross-branch military rank (paygrade)

## Enum

* `E_MINUS_1` (value: `'E-1'`)

* `E_MINUS_2` (value: `'E-2'`)

* `E_MINUS_3` (value: `'E-3'`)

* `E_MINUS_4` (value: `'E-4'`)

* `E_MINUS_5` (value: `'E-5'`)

* `E_MINUS_6` (value: `'E-6'`)

* `E_MINUS_7` (value: `'E-7'`)

* `E_MINUS_8` (value: `'E-8'`)

* `E_MINUS_9` (value: `'E-9'`)

* `E_MINUS_9_LEFT_PARENTHESIS_SPECIAL_RIGHT_PARENTHESIS` (value: `'E-9 (special)'`)

* `W_MINUS_1` (value: `'W-1'`)

* `W_MINUS_2` (value: `'W-2'`)

* `W_MINUS_3` (value: `'W-3'`)

* `W_MINUS_4` (value: `'W-4'`)

* `W_MINUS_5` (value: `'W-5'`)

* `O_MINUS_1` (value: `'O-1'`)

* `O_MINUS_2` (value: `'O-2'`)

* `O_MINUS_3` (value: `'O-3'`)

* `O_MINUS_4` (value: `'O-4'`)

* `O_MINUS_5` (value: `'O-5'`)

* `O_MINUS_6` (value: `'O-6'`)

* `O_MINUS_7` (value: `'O-7'`)

* `O_MINUS_8` (value: `'O-8'`)

* `O_MINUS_9` (value: `'O-9'`)

* `O_MINUS_10` (value: `'O-10'`)

* `SPECIAL` (value: `'Special'`)

* `SPECIAL_LEFT_PARENTHESIS_NAVY_RIGHT_PARENTHESIS` (value: `'Special (Navy)'`)

* `SPECIAL_LEFT_PARENTHESIS_COAST_GUARD_RIGHT_PARENTHESIS` (value: `'Special (Coast Guard)'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


