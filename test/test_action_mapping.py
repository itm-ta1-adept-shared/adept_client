# coding: utf-8

"""
    ITM TA1 API

    This is the specification of the TA1 API for In The Moment (ITM) for the ADEPT server. This is created in support of the Dry Run Eval. It is used to characterize and determine alignment of a decision maker's choices.

    The version of the OpenAPI document: 3.0.1
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.models.action_mapping import ActionMapping

class TestActionMapping(unittest.TestCase):
    """ActionMapping unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ActionMapping:
        """Test ActionMapping
            include_optional is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ActionMapping`
        """
        model = ActionMapping()
        if include_optional:
            return ActionMapping(
                action_id = 'action_01',
                action_type = 'APPLY_TREATMENT',
                unstructured = 'Check Mike's pulse.',
                repeatable = True,
                character_id = '',
                intent_action = True,
                threat_state = {"unstructured":"Gunfire can be heard in the surrounding area","threats":[{"type":"gunfire","severity":"severe"}]},
                parameters = [{"treatment":"Tourniquet"},{"location":"right forearm"}],
                probe_id = '',
                choice = '',
                next_scene = '',
                kdma_association = [{"Mission":0.8}],
                action_condition_semantics = 'and',
                action_conditions = openapi_client.models.conditions.Conditions(
                    elapsed_time_lt = 5, 
                    elapsed_time_gt = 5, 
                    actions = [
                        [
                            ''
                            ]
                        ], 
                    probes = ["adept-september-demo-probe-1"], 
                    probe_responses = ["adept-september-demo-probe-1-choice2"], 
                    character_vitals = [
                        openapi_client.models.conditions_character_vitals_inner.Conditions_character_vitals_inner(
                            character_id = '', 
                            vitals = openapi_client.models.vitals.Vitals(
                                avpu = 'ALERT', 
                                ambulatory = True, 
                                mental_status = 'AGONY', 
                                breathing = 'NORMAL', 
                                heart_rate = 'NONE', 
                                spo2 = 'NORMAL', ), )
                        ], 
                    supplies = [{"type":"Tourniquet","quantity":1,"reusable":false}], ),
                probe_condition_semantics = 'and',
                probe_conditions = openapi_client.models.conditions.Conditions(
                    elapsed_time_lt = 5, 
                    elapsed_time_gt = 5, 
                    actions = [
                        [
                            ''
                            ]
                        ], 
                    probes = ["adept-september-demo-probe-1"], 
                    probe_responses = ["adept-september-demo-probe-1-choice2"], 
                    character_vitals = [
                        openapi_client.models.conditions_character_vitals_inner.Conditions_character_vitals_inner(
                            character_id = '', 
                            vitals = openapi_client.models.vitals.Vitals(
                                avpu = 'ALERT', 
                                ambulatory = True, 
                                mental_status = 'AGONY', 
                                breathing = 'NORMAL', 
                                heart_rate = 'NONE', 
                                spo2 = 'NORMAL', ), )
                        ], 
                    supplies = [{"type":"Tourniquet","quantity":1,"reusable":false}], )
            )
        else:
            return ActionMapping(
                action_id = 'action_01',
                action_type = 'APPLY_TREATMENT',
                unstructured = 'Check Mike's pulse.',
                probe_id = '',
                choice = '',
        )
        """

    def testActionMapping(self):
        """Test ActionMapping"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
