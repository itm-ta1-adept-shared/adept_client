# coding: utf-8

"""
    ITM TA1 API

    This is the specification of the TA1 API for In The Moment (ITM) for the ADEPT server. This is created in support of the Dry Run Eval. It is used to characterize and determine alignment of a decision maker's choices.

    The version of the OpenAPI document: 3.0.1
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.models.get_alignment_target_api_v1_alignment_target_target_id_get200_response import GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response

class TestGetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response(unittest.TestCase):
    """GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response:
        """Test GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response
            include_optional is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response`
        """
        model = GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response()
        if include_optional:
            return GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response(
                id = '',
                kdma_values = [
                    openapi_client.models.kdma.KDMA(
                        kdma = 'denial', 
                        value = 0, 
                        kdes = openapi_client.models.kdma_kdes.KDMA_kdes(
                            rawscores = openapi_client.models.kde.KDE(
                                kde = '', ), 
                            globalnorm = openapi_client.models.kde.KDE(
                                kde = '', ), 
                            localnorm = , 
                            globalnormx_localnormy = , ), )
                    ],
                population = [
                    openapi_client.models.alignment_target.AlignmentTarget(
                        id = '', 
                        kdma_values = [
                            openapi_client.models.kdma.KDMA(
                                kdma = 'denial', 
                                value = 0, 
                                kdes = openapi_client.models.kdma_kdes.KDMA_kdes(
                                    rawscores = openapi_client.models.kde.KDE(
                                        kde = '', ), 
                                    globalnorm = openapi_client.models.kde.KDE(
                                        kde = '', ), 
                                    localnorm = , 
                                    globalnormx_localnormy = , ), )
                            ], )
                    ]
            )
        else:
            return GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response(
                id = '',
                kdma_values = [
                    openapi_client.models.kdma.KDMA(
                        kdma = 'denial', 
                        value = 0, 
                        kdes = openapi_client.models.kdma_kdes.KDMA_kdes(
                            rawscores = openapi_client.models.kde.KDE(
                                kde = '', ), 
                            globalnorm = openapi_client.models.kde.KDE(
                                kde = '', ), 
                            localnorm = , 
                            globalnormx_localnormy = , ), )
                    ],
                population = [
                    openapi_client.models.alignment_target.AlignmentTarget(
                        id = '', 
                        kdma_values = [
                            openapi_client.models.kdma.KDMA(
                                kdma = 'denial', 
                                value = 0, 
                                kdes = openapi_client.models.kdma_kdes.KDMA_kdes(
                                    rawscores = openapi_client.models.kde.KDE(
                                        kde = '', ), 
                                    globalnorm = openapi_client.models.kde.KDE(
                                        kde = '', ), 
                                    localnorm = , 
                                    globalnormx_localnormy = , ), )
                            ], )
                    ],
        )
        """

    def testGetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response(self):
        """Test GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
