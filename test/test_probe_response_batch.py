# coding: utf-8

"""
    ITM TA1 API

    This is the specification of the TA1 API for In The Moment (ITM) for the ADEPT server. This is created in support of the Dry Run Eval. It is used to characterize and determine alignment of a decision maker's choices.

    The version of the OpenAPI document: 3.0.1
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.models.probe_response_batch import ProbeResponseBatch

class TestProbeResponseBatch(unittest.TestCase):
    """ProbeResponseBatch unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ProbeResponseBatch:
        """Test ProbeResponseBatch
            include_optional is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ProbeResponseBatch`
        """
        model = ProbeResponseBatch()
        if include_optional:
            return ProbeResponseBatch(
                session_id = '',
                responses = [
                    openapi_client.models.response.Response(
                        scenario_id = 'scenario-1', 
                        probe_id = 'probe-1', 
                        choice = 's1-p1-choice1', 
                        justification = '', )
                    ]
            )
        else:
            return ProbeResponseBatch(
                session_id = '',
                responses = [
                    openapi_client.models.response.Response(
                        scenario_id = 'scenario-1', 
                        probe_id = 'probe-1', 
                        choice = 's1-p1-choice1', 
                        justification = '', )
                    ],
        )
        """

    def testProbeResponseBatch(self):
        """Test ProbeResponseBatch"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
